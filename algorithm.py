#!/usr/bin/python3
import sys
import mailbox
from tidyextractors.tidymbox import  MboxExtractor
import pandas
from Email_Processing import msg_header_text_extract, remove_nonascii, clean_header_content, write_content_to_file

def extract_features(mbox):
	#Extracts features from mbox file. Currently returns panda dataframe containing features for every email
	df = pandas.DataFrame(columns=['Message-ID', 'From', 'To', 'Subject', 'Cc', 'Return-path'])

	counter = 0
	#Appends row to dataframe for every email in mbox file
	for x in mbox:
		df = df.append({'Message-ID':x.get('Message-ID'), 
				'From':x.get('From'), 
				'To':x.get('To'), 
				'Subject':x.get('subject'), 
				'Cc':x.get('Cc'), 
				'Return-path':x.get('Return-path')}, ignore_index=True)
	return df

#Makes sure two datasets have been given as command-line argument
if len(sys.argv) != 3:
	print ('Error: Expected 2 arguments, recieved ' + str(len(sys.argv)-1) + '.')
else:
	if sys.argv[1].endswith('.mbox')==False:
		print('first arg isnt mbox')
	elif sys.argv[2].endswith('.mbox')==False:
		print('second arg isnt mbox')
	else:
		#datasets are loaded from commandline and features are extracted
		data1 = mailbox.mbox(sys.argv[1])
		data2 = mailbox.mbox(sys.argv[2])
		
		df = extract_features(data1)
		print(df)
		print(df.info())
